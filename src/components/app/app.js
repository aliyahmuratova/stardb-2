import React, {useState} from 'react';

import SwapiService from "../../services/swapi-service";
import Header from '../header';
import RandomPlanet from '../random-planet';
import {PeoplePage} from '../pages';
import {Provider} from '../swapi-context';
import './app.css';

const swapi = new SwapiService();

const App = () => {
  return (
      <Provider value={swapi}>
        <div>
          <Header />
          <RandomPlanet />
          <PeoplePage />
        </div>
      </Provider>
  );
};

export default App;
