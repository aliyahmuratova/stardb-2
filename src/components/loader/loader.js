import React from 'react';
import preloader from '../../assets/images/preloader.svg';


const Loader = () => {
    return <img src={preloader} alt="loader"/>
}

export default Loader
