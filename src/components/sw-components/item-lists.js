import React from 'react';
import ItemList from '../item-list';
import withSwapi from '../hoc';

const ItemListComp = (props) => {
    return (
       <ItemList {...props}>
           {(item) => item.name}
       </ItemList>
     )
}

const PersonList = withSwapi(
    ItemListComp,
    swapi => ({getData: swapi.getAllPeople})
);


export default PersonList
