import React, { useState } from 'react';
import {PersonList} from '../sw-components';
import Row from "../row";
import PersonDetails from "../sw-components/person-details";


const PeoplePage = () => {
    const [selectedItemId, setSelectedItemId] = useState(1)

    const leftElement = <PersonList setSelectedItemId={setSelectedItemId} />;

    const rightElement = <PersonDetails selectedItemId={selectedItemId} />;

    return <Row left={leftElement} right={rightElement} />
}


export default PeoplePage;