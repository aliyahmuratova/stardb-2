import React, {useState, useEffect,useContext} from 'react';
import Loader from "../loader/loader";
import withSwapi from '../hoc';
import './item-list.css';

const ItemList = ({ setSelectedItemId, getData, children }) => {
    const [data, setData] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getData()
            .then(data => {
                setData(data);
                setLoading (false)
            })
            .catch(error => error)
    }, [])

    const elements = data.map((person) => {
            return <li
                key={person.id}
                className="list-group-item"
                onClick={() => setSelectedItemId(person.id)}>

                {children(person)}
            </li>
        })

    if (loading) {
        return <Loader />
    }
        return(
    <ul className="item-list list-group">
        {elements}
    </ul> );
}

const divideFunction = (swapi) => ({
    getData: swapi.getAllPeople,
})

export default withSwapi(ItemList, divideFunction);