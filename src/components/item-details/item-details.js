import React, {useState, useEffect, useContext} from 'react';
import {Consumer} from '../swapi-context';
import Loader from "../loader/loader";
import './item-details.css';


const Record = ({ label, fieldName, data }) => {
  return (
      <li className="list-group-item">
        <span className="term">{label}</span>
        <span>{data[fieldName]}</span>
      </li>
  )
}

const ItemDetails = ({ selectedItemId, getData, getImage, children }) => {
  const [data, setData] = useState({
    loading: true,
    error: false,
  })
  const swapi = useContext(Consumer)

  useEffect(() => {
    getData(selectedItemId)
        .then(data => {setData({...data, loading: false, error: false})})
        .catch(error => {setData({...data, loading: false, error: true})})
  },[selectedItemId])

  const {id, name, gender, birthYear, eyeColor} = data
  const imageUrl = getImage(id)

  if (data.loading) {
    return <Loader />
  }
  if (data.error) {
    return <Loader />
  }
    return (
      <div className="person-details card">
        <img className="person-image"
          src={imageUrl} />
        <div className="card-body">
          <h4>{name}</h4>
          <ul className="list-group list-group-flush">
            {
              React.Children.map(children, (record) => {
                return React.cloneElement(record, {data: data})
              })
            }
          </ul>
        </div>
      </div>
    )
}

export {ItemDetails, Record};
